#!/usr/bin/env python

import datetime
import os
import asyncio
import websockets
import json
import time
import pandas as pd
import matplotlib.pyplot as plt

# check my external ip address to see if at work
import requests


address = "ws://localhost:8765"

print("Connecting to {}".format(address))


async def hello(iter=1, cold=False):
    async with websockets.connect(address) as websocket:
        Latency = []
        if not cold:
            for i in range(10):
                dummy_query = {"type": "echo",
                               "timestamp": time.perf_counter()}
                await websocket.send(json.dumps(dummy_query))
                response = await websocket.recv()
        print("Hot connection, Starting test")
        for i in range(iter):
            start = time.perf_counter()
            query = {"type": "echo", "timestamp": start}
            await websocket.send(json.dumps(query))
            response = await websocket.recv()
            decoded_message = json.loads(response)
            # convert the response to a float
            response = float(decoded_message["timestamp"])
            Latency.append((time.perf_counter()-response)*1000.0)

        return Latency
        # print("Latency: {}".format((time.perf_counter()-response)*1000))

# run the function 1000 times and store all the results in a pandas dataframe
results = pd.DataFrame(asyncio.run(hello(10000)))
print(results.describe())

# save the results to a csv file with a unique name based on BELL_5G_TESTING, ON_WIFI and the current time in the results folder
