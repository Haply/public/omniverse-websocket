from cgitb import handler

# from curses import halfdelay
from dataclasses import dataclass
import dataclasses
from struct import pack
from tracemalloc import start
import HaplyHardwareAPI
import serial

# list of all the ports
import serial.tools.list_ports
import time
import math
import threading

import datetime
import os
import asyncio
import websockets
import json
import time
import pandas as pd
import random
import matplotlib.pyplot as plt

# check my external ip address to see if at work
import requests

address = "ws://localhost:8765"


class Bubble_Nav:
    def __init__(self):
        self.startup()

    def startup(self):
        # initial values for bubble nav
        self.velocity_ball_center = [0, 0, 0.05]  # the center of velocity_ball
        self.workspace_center = [-0.02, -0.15, 0.1]  # center of the physical workspace #[0.04,-0.17,0.16]
        self.maxVa = 0.0001  # maximum velocity in the vel-ctl region
        self.raw_positions = [0, 0, 0.05]
        self.velocities = [0, 0, 0]
        self.forces = [0, 0, 0]
        self.start_time = time.perf_counter()
        self.scale = 3
        self.R = 0.06 #0.05
        self.ball_calc_pos = [0, 0, 0]
        self.ball_calc_pos_lock = threading.Lock()
        self.con_force = [0, 0, 0]
        self.con_force_lock = threading.Lock()

    # function to calculate the restitution force at the boundary of the pos-ctl and vel-ctl regions (acts a spring)
    def force_restitution(self, center, radius, device_pos, stiffness):
        distance = math.sqrt(sum([(device_pos[i] - center[i]) ** 2 for i in range(3)]))
        if distance < radius:
            return [0, 0, 0]
        else:
            direction = [(device_pos[i] - center[i]) / distance for i in range(3)]
            force = [direction[i] * (distance - radius) * -stiffness for i in range(3)]
            return force

    # calculates the velocity to apply to the drone if the device is in the vel-ctrl region
    def velocity_applied(self, center, radius, device_pos, Kd):
        distance = math.sqrt(sum([(device_pos[i] - center[i]) ** 2 for i in range(3)]))
        direction = [(device_pos[i] - center[i]) / distance for i in range(3)]
        velocity = [direction[i] * ((distance - radius) ** 3) * Kd for i in range(3)]
        return velocity

    # checks if the device is close to the center of the pos-ctrl region
    def start_check(self, center, device_pos, r_dead):
        distance = math.sqrt(sum([(device_pos[i] - center[i]) ** 2 for i in range(3)]))
        if distance > r_dead:
            return True
        else:
            return False

    # checks if the EE is within the pos-ctrl region
    def pos_check(self, center, device_pos, r_pos):
        distance = math.sqrt(sum([(device_pos[i] - center[i]) ** 2 for i in range(3)]))
        if distance < r_pos:
            return True
        else:
            return False

    # function to bring the inverse 3s EE into the pos ctrl region of the bubble method
    def centering(self, Inverse_object):
        start_time = self.start_time
        count = 0
        # while loop which runs until the device EE is close enough to the center of the pos-ctl region
        flag = self.start_check(self.workspace_center, self.raw_positions, 0.01)
        while flag:
            count += 1
            self.raw_positions, self.velocities = Inverse_object.end_effector_force(self.forces)
            positions = self.scale * self.raw_positions
            self.forces = self.force_restitution(self.workspace_center, 0.01, self.raw_positions, stiffness=30)
            flag = self.start_check(self.workspace_center, positions, 0.03)

            # print
            if count % 1000 == 0:
                # print("Positions: {}, Velocities: {}".format(self.raw_positions, self.velocities))
                # print("Not yet within pos region")
                dt = time.perf_counter() - start_time
                # print("frequency = {} Hz".format(1000 / dt))
                start_time = time.perf_counter()

    # function to setup the inverse 3
    def inverse3_setup(self):
        # inverse3 setup
        ports = serial.tools.list_ports.comports()
        candidates = []
        for port, desc, hwid in sorted(ports):
            print("{}: {} [{}]".format(port, desc, hwid))
            if "USB Serial" in desc:
                candidates.append(port)
        # ask the user for the port to use:
        if len(candidates) == 1:
            port = candidates[0]
        else:
            port = input("Please enter the port to use: ")
        try:
            com = HaplyHardwareAPI.SerialStream(port)
        except ValueError:
            print("Error opening port: {}".format(ValueError))
        return com

    # function to perform one step of bubble method navigation with restitution force and input contact force
    def take_step(self, count, cur_time, Inverse3, ks, start_time):
        h = time.perf_counter() - cur_time
        cur_time = time.perf_counter()

        self.con_force_lock.acquire()
        con_for = self.con_force
        self.con_force_lock.release()
        # con_for = [0, 0, 0]
        scale = -100
        con_for = [scale * con_for[0], scale * con_for[1], scale * con_for[2]]

        # inverse data fetching and handling
        self.forces = [self.forces[i] + con_for[i] for i in range(3)]
        self.raw_positions, self.velocities = Inverse3.end_effector_force(self.forces)
        positions = self.scale * [
            (self.raw_positions[i] - self.workspace_center[i]) for i in range(3)
        ]  # if raw_positions = [0,0,0], sets positions to the center of the workspace

        # bubble method forces
        restitution_forces = self.force_restitution(self.workspace_center, self.R, self.raw_positions, ks)
        self.forces = [restitution_forces[i] for i in range(3)]

        # bubble method region checking and handling
        if self.pos_check(self.workspace_center, self.raw_positions, self.R) is False:  # drone is in the vel-ctl region
            Va = self.velocity_applied(
                self.workspace_center, self.R, self.raw_positions, Kd=100
            )  # get the velocity of movement in vel-ctl region
            magVa = math.sqrt(Va[0] * Va[0] + Va[1] * Va[1] + Va[2] * Va[2])
            if magVa < self.maxVa:  # if the velocity of the drone is less than the maxVa (set above)
                self.velocity_ball_center = [(self.velocity_ball_center[i] + Va[i]) for i in range(3)]
            else:  # otherwise if the drone is >= the maxVa
                Va = [(self.maxVa / magVa) * Va[0], (self.maxVa / magVa) * Va[1], (self.maxVa / magVa) * Va[2]]
                # update the velocity_ball_center to move in the direction of the velocity of the drone
                self.velocity_ball_center = [(self.velocity_ball_center[i] + Va[i]) for i in range(3)]

        # update the positions of the drones set point
        calc_pos = [positions[i] + self.velocity_ball_center[i] for i in range(3)]

        self.ball_calc_pos_lock.acquire()
        self.ball_calc_pos = calc_pos
        self.ball_calc_pos_lock.release()

        # print
        if count % 1000 == 0:
            # print(con_for)
            # print(self.forces)
            # print("Positions: {}, Velocities: {}".format(
            # positions, self.velocities))
            dt = time.perf_counter() - start_time
            # print("frequency = {} Hz".format(1000 / dt))
            start_time = time.perf_counter()

        # return state variables
        return cur_time

    def Inverse_loop(self, Inverse3):
        # sets some initial values for a few variables
        ks = 200
        count = 0
        cur_time = time.perf_counter()
        start_time = time.perf_counter()

        while True:
            count += 1
            # take a step in the bubble method
            cur_time = self.take_step(count, cur_time, Inverse3, ks, start_time)

    # main runtime function
    def Web_sock_server(self, iteration):
        async def web_sock(self, iter):
            async with websockets.connect(address) as websocket:
                Latency = []
                dt = 0.001
                pos = [0, 0, 0]

                while True:

                    self.ball_calc_pos_lock.acquire()
                    calc_pos = self.ball_calc_pos
                    self.ball_calc_pos_lock.release()

                    # convert from normal XYZ cords to Omniverse ZXY cords
                    pos[0] = calc_pos[0]
                    pos[1] = calc_pos[1]
                    pos[2] = calc_pos[2]

                    # send the pos data to omniverse an fetch any contact data
                    start = time.perf_counter()
                    query = {"type": "haptic", "timestamp": start, "data": pos}
                    await websocket.send(json.dumps(query))
                    response = await websocket.recv()
                    decoded_message = json.loads(response)
                    con_for = decoded_message["force"]

                    self.con_force_lock.acquire()
                    self.con_force = con_for
                    self.con_force_lock.release()

                    # convert the response to a float
                    response = float(decoded_message["timestamp"])
                    dt = time.perf_counter() - response
                    # print("dt: {}".format(dt))
                return Latency

        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        asyncio.run(web_sock(self, iteration))


# ---------- MAIN CODE STARTS HERE -------------------

Bubble_Navigation = Bubble_Nav()

# sets up the inverse 3 object and connects to device
comm = Bubble_Navigation.inverse3_setup()
Inverse3 = HaplyHardwareAPI.Inverse3(comm)
Response = Inverse3.device_wakeup()
print(Response)
print("Made Handshake with device")

# sets up the websocket stuff
print("Connecting to {}".format(address))

# brings the inverse into the pos-ctrl region of the bubble nav
Bubble_Navigation.centering(Inverse3)

# run both threads
print("starting main loop\n")
Inverse_thread = threading.Thread(target=Bubble_Navigation.Inverse_loop, args=(Inverse3,))

Server_thread = threading.Thread(target=Bubble_Navigation.Web_sock_server, args=(100000,))

Inverse_thread.start()

Server_thread.start()
Server_thread.join()
