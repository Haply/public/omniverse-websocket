#!/usr/bin/env python

import datetime
import os
import asyncio
import websockets
import json
import time
import pandas as pd
import random
import matplotlib.pyplot as plt

# check my external ip address to see if at work
import requests


address = "ws://localhost:8766"

print("Connecting to {}".format(address))

# simulation of a drone flying in a 2m x 2m x 2m cube and bouncing off the walls


def drone_sim(current_pos, current_vel, dt=0.001, cube_size=2.0):
    new_pos = [0, 0, 0]
    new_vel = [0, 0, 0]
    for i in range(3):
        new_pos[i] = current_pos[i] + current_vel[i] * dt
        new_vel[i] = current_vel[i]
        if new_pos[i] > cube_size / 2:
            new_pos[i] = cube_size / 2
            new_vel[i] = -1 * current_vel[i]
        if new_pos[i] < -1 * cube_size / 2:
            new_pos[i] = -1 * cube_size / 2
            new_vel[i] = -1 * current_vel[i]
    return new_pos, new_vel


async def hello(iter=1):
    async with websockets.connect(address) as websocket:
        Latency = []
        pos = [0, 0, 0]
        # define a random velocity between 0 and 1 m/s
        vel = [random.uniform(0, 1), random.uniform(0, 1), random.uniform(0, 1)]
        dt = 0.001
        for i in range(iter):
            start = time.perf_counter()
            pos, vel = drone_sim(pos, vel)
            query = {"type": "haptic", "timestamp": start, "data": pos}
            await websocket.send(json.dumps(query))
            response = await websocket.recv()
            decoded_message = json.loads(response)
            print(decoded_message)
            query = {"type": "drone", "timestamp": start, "data": [0, 0, 0]}
            await websocket.send(json.dumps(query))
            response = await websocket.recv()
            decoded_message = json.loads(response)
            # print(decoded_message)
            # get multiple responses

            # convert the response to a float
            response = float(decoded_message["timestamp"])
            dt = time.perf_counter() - response
            # print("dt: {}".format(dt))

        return Latency
        # print("Latency: {}".format((time.perf_counter()-response)*1000))


# run the function 1000 times and store all the results in a pandas dataframe
asyncio.run(hello(100000))


# save the results to a csv file with a unique name based on BELL_5G_TESTING, ON_WIFI and the current time in the results folder
