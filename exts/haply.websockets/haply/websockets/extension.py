import omni.ext
import omni.ui as ui
import websockets
import asyncio
import threading
import json
import os
from pxr import PhysxSchema, PhysicsSchemaTools
from pxr import UsdGeom, UsdUtils, UsdPhysics, UsdShade
from pxr import Usd, Gf, Sdf

import carb

# default kit unit is cm, so we need to convert to m
conversion_unit = 1

# Any class derived from `omni.ext.IExt` in top level module (defined in `python.modules` of `extension.toml`) will be
# instantiated when extension gets enabled and `on_startup(ext_id)` will be called. Later when extension gets disabled
# on_shutdown() is called.


def get_haply_asset_path(resource_relative_path: str) -> str:
    """
    Helper to construct an asset path to the demo asset location based on setting '/physics/demoAssetsPath'

    Args:
        path: String path relative to root folder of the demo assets.

    Example:
        Get path to sphere_high.usd:
        asset_path = get_demo_asset_path("common/sphere_high.usd")
    """
    # assets_path_setting = carb.settings.get_settings().get("/physics/haplyAssetsPath")
    # assert assets_path_setting, "Empty string in setting /physics/haplyAssetsPath!"
    assets_path_setting = os.path.dirname(os.path.realpath(__file__))
    # get two level up from the current path
    assets_path_setting = os.path.dirname(os.path.dirname(assets_path_setting))
    # join path with the relative path
    asset_path = os.path.join(assets_path_setting, "assets", resource_relative_path)
    return asset_path


class MyExtension(omni.ext.IExt):
    def __init__(self):
        super().__init__()
        self._setup_callbacks()

    # ext_id is current extension id. It can be used with extension manager to query additional information, like where
    # this extension is located on filesystem.
    def on_startup(self, ext_id):
        print("[haply.websockets] MyExtension startup")
        self.stop_thread = False
        self.drone_position = [0, 0, 0]
        self.sim_drone_position = [0, 0, 0]
        self.drone_position_lock = threading.Lock()
        self.haptic_position = [0, 0, 0]
        self.haptic_force = [0, 0, 0]
        self.haptic_position_lock = threading.Lock()
        self._window = ui.Window("Websockets management", width=300, height=300)
        with self._window.frame:

            with ui.VStack():
                with ui.CollapsableFrame("Parameters", name="group"):
                    # create a default port input and a button to start the websocket server
                    with ui.HStack():
                        ui.Label("Port:")
                        self._port_input = ui.IntField(name="server_port")
                        self._port_input.model.as_int = 8765
                        self._start_button = ui.Button("Start", name="start_button", clicked_fn=self._start_server)
                        self._stop_button = ui.Button("Stop", name="stop_button", clicked_fn=self._stop_server)
                ui.Line()
                ui.Label("Server status:")
                self._status_label = ui.Label("Not running")

                # add a button to create the drone object in the scene
                ui.Button("UR10 demo shortcut", name="init_demo", clicked_fn=self._init_demo)
                ui.Button("Create drone", name="create_drone", clicked_fn=self._create_drone)
                ui.Button("Create workspace", name="create_workspace", clicked_fn=self._create_workspace_cube)
                ui.Button("Create haptic end-effector", name="create_haptic_ee", clicked_fn=self._create_haptic_ee)
                # button to create the fixed joint between the drone and the haptic end-effector
                ui.Button("Create fixed joint", name="create_fixed_joint", clicked_fn=self._create_fixed_joint)

    def _init_demo(self):
        self._create_drone()
        self._create_haptic_ee()
        self._create_fixed_joint()

        #hide dronebox and haptic ee
        usd_context = omni.usd.get_context()
        stage = usd_context.get_stage()
        DronePrim = stage.GetPrimAtPath("/Drone/DroneBox")
        HapticEEPrim = stage.GetPrimAtPath("/HapticEE/HapticEE")
        if DronePrim and HapticEEPrim:
            # set visibility to invisible
            DronePrim.GetAttribute("visibility").Set("invisible")
            HapticEEPrim.GetAttribute("visibility").Set("invisible")
        # start the websocket server
        self._start_server()
        # change camera position to X: 4.15, Y: -0.13, Z: 0.17 and orientation to X: -22, Y: 80, Z: 112
        camera_prim1 = stage.DefinePrim("/World/Camera1", "Camera")
        Position = Gf.Vec3f(4.15, -0.13, 0.17)
        UsdGeom.Xformable(camera_prim1).AddTranslateOp().Set(Position)
        UsdGeom.Xformable(camera_prim1).AddRotateXYZOp().Set((80, 0, 95))

        # focal length 18
        camera_prim1.GetAttribute("focalLength").Set(18)
        # switch to camera 1
        viewport = omni.kit.viewport_legacy.get_viewport_interface()

        # acquire the viewport window
        viewport_handle = viewport.get_instance("Viewport")
        viewport_window = viewport.get_viewport_window(viewport_handle)
        viewport_window.set_active_camera("/World/Camera1")

        # run physics scene at 80fps
        scene = UsdPhysics.Scene.Get(stage, "/physics/scene")
        physxSceneAPI = PhysxSchema.PhysxSceneAPI.Apply(scene.GetPrim())
        physxSceneAPI.GetTimeStepsPerSecondAttr().Set(80)



    def _create_fixed_joint(self):
        # create a fixed joint between the drone and the haptic end-effector
        usd_context = omni.usd.get_context()
        stage = usd_context.get_stage()
        DronePrim = stage.GetPrimAtPath("/Drone/DroneBox")
        HapticEEPrim = stage.GetPrimAtPath("/HapticEE/HapticEE")
        if DronePrim and HapticEEPrim:
            FixedJoint = UsdPhysics.FixedJoint.Define(stage, "/HapticEE/HapticEE/FixedJoint")
            FixedJoint.CreateBody0Rel().SetTargets(["/Drone/DroneBox"])
            FixedJoint.CreateBody1Rel().SetTargets(["/HapticEE/HapticEE"])

    def on_physics_step(self, dt):
        MainPrim = omni.usd.get_context().get_stage().GetPrimAtPath("/Drone/DroneBox")
        # get the position of the drone
        if MainPrim:
            drone_position_sim = MainPrim.GetAttribute("xformOp:translate").Get()
        #        if MainPrim:
        #            drone_position_real = MainPrim.GetAttribute("xformOp:translate")
        # make the visual drone follow the simulated drone
        MainPrim = omni.usd.get_context().get_stage().GetPrimAtPath("/Drone/DroneVisu/S9_Mini_Drone")
        if MainPrim:
            MainPrim.GetAttribute("xformOp:translate").Set(drone_position_sim)
        self.drone_position_lock.acquire()
        drone_position = self.drone_position
        # convers from list to GfVec3d
        drone_position = Gf.Vec3f(drone_position[0], drone_position[1], drone_position[2])
        # convert from cm to m
        drone_position = drone_position * conversion_unit
        # GfVec3D to List
        self.sim_drone_position = [drone_position_sim[0], drone_position_sim[1], drone_position_sim[2]]
        self.drone_position_lock.release()
        self.haptic_position_lock.acquire()
        haptic_position = self.haptic_position
        haptic_position = Gf.Vec3f(haptic_position[0], haptic_position[1], haptic_position[2])
        haptic_position = haptic_position * conversion_unit
        force = haptic_position - drone_position_sim
        self.haptic_force = [force[0], force[1], force[2]]
        # convers from list to GfVec3d

        self.haptic_position_lock.release()
        usd_context = omni.usd.get_context()
        stage = usd_context.get_stage()
        MainPrim = stage.GetPrimAtPath("/HapticEE")
        if MainPrim:
            MainPrim.GetAttribute("xformOp:translate").Set(haptic_position)

    def _on_physics_step(self, dt):
        self.on_physics_step(dt)

    def _setup_callbacks(self):
        self._physics_update_sub = omni.physx.get_physx_interface().subscribe_physics_step_events(self._on_physics_step)

    def _create_drone(self):
        # create a drone object in the scene
        usd_context = omni.usd.get_context()
        stage = usd_context.get_stage()
        MainPrim = stage.GetPrimAtPath("/Drone")
        if not MainPrim:
            MainPrim = UsdGeom.Xform.Define(stage, "/Drone")
            MainPrim.AddTranslateOp()
            MainPrim = stage.GetPrimAtPath("/Drone")
            drone_position = Gf.Vec3d(0, 0, 0)
            MainPrim.GetAttribute("xformOp:translate").Set(drone_position)
        DroneBox = UsdGeom.Cube.Define(stage, "/Drone/DroneBox")
        DroneBox.GetSizeAttr().Set(0.15 * conversion_unit)
        PhysicalDrone = UsdPhysics.RigidBodyAPI.Apply(DroneBox.GetPrim())
        # PhysicalDrone.CreateMassAttr().Set(1)
        UsdPhysics.CollisionAPI.Apply(DroneBox.GetPrim())
        # add material
        mtl_created_list = []
        omni.kit.commands.execute(
            "CreateAndBindMdlMaterialFromLibrary",
            mdl_name="http://omniverse-content-production.s3-us-west-2.amazonaws.com/Materials/Base/Glass/Red_Glass.mdl",
            mtl_name="Red_Glass",
            mtl_created_list=mtl_created_list,
        )
        mtl_prim = stage.GetPrimAtPath(mtl_created_list[0])
        drone_shade = UsdShade.Material(mtl_prim)
        UsdShade.MaterialBindingAPI(DroneBox.GetPrim()).Bind(drone_shade)
        visu_drone = get_haply_asset_path("drone.usd")
        visu_path = MainPrim.GetPath().AppendChild("DroneVisu")
        stage.DefinePrim(visu_path).GetReferences().AddReference(visu_drone)
        print(visu_drone)

    def _create_haptic_ee(self):
        # create a haptic end-effector representing the haptic device
        usd_context = omni.usd.get_context()
        stage = usd_context.get_stage()
        MainPrim = stage.GetPrimAtPath("/HapticEE")
        if not MainPrim:
            MainPrim = UsdGeom.Xform.Define(stage, "/HapticEE")
            MainPrim.AddTranslateOp()
            MainPrim = stage.GetPrimAtPath("/HapticEE")
            haptic_position = Gf.Vec3d(0, 0, 0)
            MainPrim.GetAttribute("xformOp:translate").Set(haptic_position)
        HapticEE = UsdGeom.Sphere.Define(stage, "/HapticEE/HapticEE")
        HapticEE.GetRadiusAttr().Set(0.1 * conversion_unit)
        PhysicalHaptic = UsdPhysics.RigidBodyAPI.Apply(HapticEE.GetPrim())
        PhysicalHaptic.CreateKinematicEnabledAttr().Set(True)
        # set the material
        mtl_created_list = []
        omni.kit.commands.execute(
            "CreateAndBindMdlMaterialFromLibrary",
            mdl_name="http://omniverse-content-production.s3-us-west-2.amazonaws.com/Materials/Base/Metals/Aluminum_Cast.mdl",
            mtl_name="Aluminum_Cast",
            mtl_created_list=mtl_created_list,
        )
        mtl_prim = stage.GetPrimAtPath(mtl_created_list[0])
        haptic_shade = UsdShade.Material(mtl_prim)
        UsdShade.MaterialBindingAPI(HapticEE.GetPrim()).Bind(haptic_shade)

    def _create_workspace_cube(self, size=2):
        mtl_created_list = []
        # Create a new material using OmniGlass.mdl
        omni.kit.commands.execute(
            "CreateAndBindMdlMaterialFromLibrary",
            mdl_name="http://omniverse-content-production.s3-us-west-2.amazonaws.com/Materials/Base/Glass/Tinted_Glass_R85.mdl",
            mtl_name="Tinted_Glass_R85",
            mtl_created_list=mtl_created_list,
        )
        # create a transparent cube to represent the workspace
        usd_context = omni.usd.get_context()
        stage = usd_context.get_stage()
        MainPrim = stage.GetPrimAtPath("/Workspace")
        if not MainPrim:
            MainPrim = UsdGeom.Xform.Define(stage, "/Workspace")
            MainPrim.AddTranslateOp()
        WorkspaceBox = UsdGeom.Cube.Define(stage, "/Workspace/WorkspaceBox")
        WorkspaceBox.GetSizeAttr().Set(size * conversion_unit)
        mtl_prim = stage.GetPrimAtPath(mtl_created_list[0])
        omni.usd.create_material_input(mtl_prim, "glass_color", Gf.Vec3f(0, 1, 0), Sdf.ValueTypeNames.Color3f)
        cube_shade = UsdShade.Material(mtl_prim)
        UsdShade.MaterialBindingAPI(WorkspaceBox.GetPrim()).Bind(cube_shade)
        # add a ground plane
        PhysicsSchemaTools.addGroundPlane(
            stage,
            "/Workspace/GroundPlane",
            "Y",
            25 * conversion_unit,
            Gf.Vec3f(0, -1 * conversion_unit, 0),
            Gf.Vec3f(1.0),
        )

    def _stop_server(self):
        self.stop_thread = True
        self._status_label.text = "Not running"
        self._server_thread.join()

    def _start_server(self):
        # force quit the previous server thread if it was running
        self.stop_thread = True
        if hasattr(self, "_server_thread") and self._server_thread.is_alive():
            self._server_thread.join()
        # start a new thread to run the server with the given port
        self.stop_thread = False
        self._server_thread = threading.Thread(
            target=self.WebSocketServer, args=(self._port_input.model.as_int, lambda: self.stop_thread)
        )
        self._server_thread.start()
        self._status_label.text = "Running"

    def on_shutdown(self):
        print("[haply.websockets] MyExtension shutdown")

    def WebSocketServer(self, port=8765, stop_event=None):
        async def server_task(websocket, path):
            async for message in websocket:
                decoded_message = json.loads(message)
                if decoded_message["type"] == "echo":
                    await websocket.send(message)
                elif decoded_message["type"] == "drone":
                    # print(decoded_message["data"])
                    self.drone_position_lock.acquire()
                    self.drone_position = decoded_message["data"]
                    self.drone_position_lock.release()
                    message = json.dumps(
                        {
                            "type": "drone",
                            "timestamp": decoded_message["timestamp"],
                            "real_pos": self.sim_drone_position,
                        }
                    )
                    await websocket.send(message)
                elif decoded_message["type"] == "haptic":
                    # print(decoded_message["data"])
                    self.haptic_position_lock.acquire()
                    self.haptic_position = decoded_message["data"]
                    message = json.dumps(
                        {"type": "haptic", "timestamp": decoded_message["timestamp"], "force": self.haptic_force}
                    )
                    self.haptic_position_lock.release()
                    await websocket.send(message)
                else:
                    print(message)

        async def server_loop(port):
            async with websockets.serve(server_task, "0.0.0.0", port):
                print(f"Server started on port {port}")
                while True:
                    await asyncio.sleep(1)
                    # print("Server running")
                    if stop_event():
                        break

        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        asyncio.run(server_loop(port))
